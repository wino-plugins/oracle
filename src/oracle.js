const OracleDB = require('oracledb');
const {FileUtils} = require('wiplug-utils');

OracleDB.outFormat = OracleDB.OUT_FORMAT_OBJECT;
global.oracle_connections = {}

async function getConfigs(){
   try {
      if(await FileUtils.getFileStats('./configs/oracle.json')){
         let data = await FileUtils.readFileJSON('./configs/oracle.json')
         return data
      }
   } catch (err) {
      throw err
   }
}

async function connectToDbs(){
   try {
      let configs = await getConfigs()

      for (let i = 0; i < configs.dbs.length; i++) {
         const db = configs.dbs[i];
         
         oracle_connections[db.name] = {
            connection: await OracleDB.getConnection({
               user: db.username,
               password: db.password,
               connectString : `${db.host}:${db.port}/${db.sid}`,
            }),
            db
         }

         console.log(chalk.green(`[x] Connected to oracle db: ${db.name}`))
      }

   } catch (err) {
      throw err
   }
}

module.exports = {
   connectToDbs,
}